The people listed below all publicly support the [Fair Open Access Principles](https://www.fairopenaccess.org/). The list is not exhaustive and reflects the resources of the people creating it. 

The principles are:

1. The journal has a transparent ownership structure, and is controlled by and responsive to the scholarly community.
2. Authors of articles in the journal retain copyright.
3. All articles are published open access and an explicit open access licence is used.
4. Submission and publication is not conditional in any way on the payment of a fee from the author or its employing institution, or on membership of an institution or society.
5. Any fees paid on behalf of the journal to publishers are low, transparent, and in proportion to the work carried out.

French version / version Française:

1. le journal a un ou des propriétaires clairement identifiés, il est contrôlé par la communauté académique et lui rend compte ;
2. les auteurs gardent la propriété intellectuelle complète des articles ;
3. tous les articles sont publiés en accès libre garanti par une licence explicite ;
4. la soumission et la publication ne sont conditionnées à aucun paiement par les auteurs ni par l'institution les employant, non plus qu'à leur appartenance à une société savante ou à une institution ;
5. tous les frais payés à des fournisseurs de service pour la publication du journal sont modérés, transparents, et en proportion du travail fourni.


Mathematics/Statistics
----------------

* Adem, Alejandro - University of British Columbia
* Agol, Ian - University of California Berkeley
* Alon, Noga - Princeton University and Tel Aviv University
* Arnold, Douglas - University of Minnesota
* Atiyah, Michael - University of Edinburgh
* Baez, John - University of California Riverside
* Ball, John - University of Oxford
* Barcelo, Hélène - Mathematical Sciences Research Institute
* Bass, Hyman - University of Michigan
* Beck, Matthias - San Francisco State University
* Brent, Richard - Australian National University
* Bressoud, David - Macalester College
* Bugeaud, Yann - Université de Strasbourg
* Calegari, Danny - University of Chicago
* Chambert-Loir, Antoine - Université Paris Diderot
* Chafaï,  Djalil -  Université Paris-Dauphine
* Cohn, Henry -  Microsoft Research
* Demailly, Jean-Pierre - Université Grenoble Alpes
* Ellenberg, Jordan - University of Wisconsin
* Emerton, Matthew - University of Chicago
* Evans, Lawrence C. - University of California Berkeley
* Farge, Marie - École Normale Supérieure and CNRS
* Gee, Toby -  Imperial College London
* Gowers, Timothy - University of Cambridge
* Grötschel, Martin - Berlin-Brandenburgische Akademie der Wissenschaften
* Hairer, Martin - Imperial College London
* Hales, Thomas -  University of Pittsburgh
* Harris, Michael - Columbia University and Université Paris-Diderot
* Hélein, Frédéric - Université Paris-Diderot
* Hersh, Reuben - University of New Mexico
* Iserles, Arieh - University of Cambridge
* Iwaniec, Tadeusz - Syracuse University
* Joshi, Nalini - University of Sydney
* Joyal, André - Université du Québec à Montréal
* Karoubi, Max - Université Paris Diderot
* Kohn, Joseph J. - Princeton University
* Kuperberg, Krystyna - Auburn University
* Lafforgue, Vincent - Université de Grenoble and CNRS
* Lafforgue, Laurent - Institut des Hautes Études Scientifiques
* May, J. Peter - University of Chicago
* Meyer, Yves - École Normale Supérieure Paris-Saclay
* Morrison, Scott - Australian National University
* Mouhot, Clément  - University of Cambridge
* Mumford, David - Brown University
* Odlyzko, Andrew - University of Minnesota
* Olver, Peter - University of Minnesota
* Palais, Richard - University of California Irvine
* Pardon, John - Princeton University
* Reiner, Victor - University of Minnesota
* Solovay, Robert - University of California Berkeley
* Stein, Elias - Princeton University
* Taylor, Richard - Institute for Advanced Study
* Voloch, Felipe - University of Canterbury
* Wolf, Julia - University of Bristol
* Wooley, Trevor - University of Bristol
* Zeilberger, Doron - Rutgers University
* Zeitouni, Ofer - Weizmann Institute of Science
* Ziegler, Günter - Free University of Berlin 

Computer Science
-------------
* Apt, Krzysztof - CWI Amsterdam
* Baeten, Jos - CWI Amsterdam
* Eppstein, David - University of California Irvine
* Erickson, Jeff - University of Illinois
* Halpern, Joe - Cornell University
* LeCun, Yann - New York University and Facebook
* Salvy, Bruno - INRIA and ENS Lyon

Psychology / Neuroscience
--------------
* Ansari, Daniel - Western Ontario
* Brady, Tim -	UC San Diego
* Carey, Susan - Harvard
* Creel, Sarah - UCSD
* Dehaene, Stanislas - Collège de France / INSERM
* Dunham, Yarrow -	Yale University
* Gallistel, Randy -	Rutgers University
* Goldberg, Adele -	Princeton University
* Goodman Noah - Stanford University
* Gopnik, Alison -	UC Berkeley
* Kemp, Charles -	Carnegie Mellon University
* Lombrozo, Tania - UC Berkeley
* Marcus, Gary -	NYU
* Kanwisher, Nancy - MIT
* Legare, Cristine - UT Austin
* Lombrozo, Tania -	UC Berkeley
* Mahon, Bradford -	University of Rochester
* McDermott, Josh - MIT
* Pashler, Hal - University of California San Diego
* Perfors, Amy - University of Melbourne
* Piantadosi, Steve -	University of Rochester
* Reder, Lynn - CMU
* Saffran, Jenny -	University of Wisconsin-Madison
* Spelke, Elizabeth	-	Harvard University
* Santos, Laurie -	Yale University
* Saxe, Rebecca -	MIT
* Rooryck, Johan -	Leiden University
* Vul, Ed - UCSD
* Wagenmakers, Eric-Jan - University of Amsterdam

Physics
-------

* Caux, Jean-Sébastien - University of Amsterdam