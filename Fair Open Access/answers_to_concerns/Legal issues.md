Typically the editors-in-chief of a commercially owned journal have explicit contracts which prohibit the editors engaging in 
competition with the journal, and require substantial (often 6 months) notice of resignation to be given. Thus it is important for the EiCs to keep at arms' length from the new journal.

The easiest solution is for the new journal to name some interim EiCs for a short time until the EiCs can resume their position. This is exactly what was done with Glossa and Algebraic Combinatorics. Note that this was done in consultation with an IP lawyer provided by LingOA/MathOA. Typically, the ordinary editorial board members  have no contract and are free to do whatever they like. Of course, if they have contracts, these should be looked at by our lawyer.


