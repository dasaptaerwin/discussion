Basically a full-service operation, the one behind the Open Library of Humanities and more than 20 other university presses. Costs range from the hundreds of dollars/euros per year (for hosting of the journal and hardware and software maintenance, with minimal additional services) to the hundreds of dollars/euros per article (for a complete process, including staff time to support the peer-review process, XML and PDF typesetting and copyediting, full production). Intermediate prices are possible, the services can be personalised feature by feature. The editorial system is modern, based on Open Journal Systems, but nicer. They now have experience of transferring/flipping journals from all major publishers.

Ubiquity Press also supports alternative research outputs such as data and software, either in their own 'metajournals' or as data/software papers within standard journals.

The company is a spinoff from University College London: http://www.ubiquitypress.com/, and now has offices in London and Berkeley.

More details:

* [Example journal publishing with Ubiquity] (http://www.glossa-journal.org) (part of https://www.openlibhums.org/)
* [Example of a metajournal] (https://openresearchsoftware.metajnl.com/)
* [An editor training video] (https://vimeo.com/144631620) (password: JMSTraining)

Two main levels of service:

* Hosted journals: Ubiquity Press simply hosts the journals on our platform and ensures that they are fully functional and available. The client institution is responsible for editorial support and ensuring that functions such as peer review, copyediting and typesetting are carried out. These journals have a flat annual fee of £750 or ca. €850.

* Full-service journals: Here we provide a full stack of publishing services, including dedicated editorial support, anti-plagiarism checking, copyediting, typesetting, indexing and archiving and content promotion, etc.. These journals have no annual charge, but do incur a fee per article published of £400/ca.€450  (HSS) or £500/ca.€565 (STEM). These fees are generally paid by either the Society, or by the author's funder or institution [Note by MathOA: compliance with Fair OA principle necessarily entails that the APC are paid journal-side, e.g. by MathOA, rather than author-side]

* Optional services: Both hosted and full service journals can elect to use additional services. A hosted journal for example may like to add typesetting, while full service journal may opt for more advanced copyediting or an alternative editorial management system. These services are charged on either an annual or per-article basis, as appropriate.
