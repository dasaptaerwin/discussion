This directory contains resources for those wanting to start a new journal complying with Fair Open Access Principles.

0. Justify the journal
1. Obtain funding
2. Decide on journal governance
3. Decide on journal policies

* [Open access licences] (https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Licences.md)

4. Recruit editorial board
5. Decide on service provider

* [Service providers] (https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/Details%20of%20publishing%20service%20providers.md)
 
6. Advertise journal
7. Upskill editors
8. Apply for indexing

